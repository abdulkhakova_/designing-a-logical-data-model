CREATE TABLE Climber_Details (
    climber_ID int,
    First_Name varchar(255),
    Surname varchar(255),
    Age int, 
    Phone_number int,
    Address varchar(255),
    City varchar(255)
     PRIMARY KEY (climber_ID)
);
CREATE TABLE  Mountain_Details (
    mountain_ID int,
    Name_of_the_mountain varchar(255),
    Location varchar(255),
    Height int,
     PRIMARY KEY (mountain_ID)
);
CREATE TABLE  Region_Details (
    region_ID int,
    Region_name varchar(255),
     PRIMARY KEY (region_ID)
);
CREATE TABLE Country_Details (
    country_ID int,
    country_name varchar(255),
    PRIMARY KEY (country_ID)
);
CREATE TABLE Climb_Dates (
    climb_ID int,
    start_date int,
    end_date int,
    PRIMARY KEY (climb_ID)
);
CREATE TABLE Climber_Mountain_Link (
     climber_ID int,
     mountain_ID int,
     FOREIGN KEY (climber_ID)
     FOREIGN KEY (mountain_ID)
);
CREATE TABLE  Mountain_Region_Link (
    mountain_ID int,
    region_ID int, 
    FOREIGN KEY (mountain_ID)
    FOREIGN KEY(region_ID)
);
CREATE TABLE  Country_Region_Link (
    country_ID int,
    region_ID int,
    FOREIGN KEY (country_ID)
    FOREIGN KEY (region_ID)
);
CREATE TABLE Climb_Climber_Link (
    climb_ID int,
    climber_ID int,
    FOREIGN KEY (climb_ID)
    FOREIGN KEY (climber_ID)
);
CREATE TABLE Climb_Mountain_Link (
     climb_ID int,
     mountain_ID int,
     FOREIGN KEY (climb_ID)
     FOREIGN KEY (mountain_ID)

);
